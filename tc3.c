//do not use any optimization switch (like O3,...) when you generate llvm bitcodes

#include <stdio.h>

void foo() { printf("foo\n");}
void bar() { printf("bar\n");}
void baz() { printf("baz\n");}

int main(int argc, char** argv) {

  int x = 5;
  switch (x) {
    case 0:
      foo();
      break;
    case 5:
      bar();
      break;
    default:
      baz();
  }
}
